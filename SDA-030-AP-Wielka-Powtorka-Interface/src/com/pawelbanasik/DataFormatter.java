package com.pawelbanasik;

public interface DataFormatter {
    void format (Person person);
    void format (Car car);
}
